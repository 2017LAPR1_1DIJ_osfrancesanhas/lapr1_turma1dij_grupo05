package lapr1projetofinal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExecutionTimeInsertion {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException {
        long lStartTime = System.currentTimeMillis();
        calculation();
        long lEndTime = System.currentTimeMillis();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
    }
    private static void calculation() throws InterruptedException, FileNotFoundException {
        lerMatriz();
    }
        
    public static void lerMatriz() throws FileNotFoundException {
        Scanner fInput = new Scanner(new File("testImage200.txt"));
        String linha0 = fInput.nextLine();
        String[] linhaDiv = linha0.split("  ");
        int size = Integer.parseInt(linhaDiv[1]);
        int[][] matriz = new int[size][size];
        fInput.nextLine();
        for (int i = 0; i < size; i++) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            String[] temp = linha.split(",");
            for (int j = 0; j < size; j++) {
                matriz[i][j] = Integer.parseInt(temp[j]);
            }
        }
        int[] vetorTotal = matrizParaVetor(matriz, size);
        int[] vetorOrde = doInsertionSort(vetorTotal);
        vetorParaMatriz(vetorOrde, size);
    }

    public static int[] doInsertionSort(int[] input){
         
        int temp;
        for (int i = 1; i < input.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(input[j] < input[j-1]){
                    temp = input[j];
                    input[j] = input[j-1];
                    input[j-1] = temp;
                }
            }
        }
        return input;
    }

    public static int[] matrizParaVetor(int matriz[][], int size) {
        int[] vetor = new int[size * size];
        int cont = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                vetor[cont] = matriz[i][j];
                cont = cont + 1;
            }
        }
        return vetor;
    }

    public static void vetorParaMatriz(int[] vetor, int size) {
        int[][] matrizTransformada = new int[size][size];
        int contador = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizTransformada[i][j] = vetor[contador];
                contador++;
            }
        }
        listarMatriz(matrizTransformada);
    }

    public static void listarMatriz(int[][] matriz) {
        System.out.println("Listagem da matriz");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}