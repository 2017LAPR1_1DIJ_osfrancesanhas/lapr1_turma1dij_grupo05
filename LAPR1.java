package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import static lapr1.Constantes.*;

public class LAPR1 {

    public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        String linha = lerFicheiro();
        String[] título = linha.split("  ");
        String nome = título[0];
        int size = Integer.parseInt(título[1]);
        int[][] matriz = new int[size][size];
        String nomeFich = título[2];
        lerMatriz(nomeFich, size, matriz);
        int menu;

        do {
            menu = menu();
            switch (menu) {
                case 1:
                    int menuEstatísticas;
                    menuEstatísticas = menuEstatísticas();
                    break;

                case 2:
                    int menuTransformações;
                    menuTransformações = menuTransformações();
                    break;

                case 0:
                    System.out.println("Obrigado!");
                    break;
            }

        } while (menu != 0);

    }

    public static int menu() {
        System.out.print("Escolha uma opção:\n"
                + "1 - Caracterização da imagem\n"
                + "2 - Filtragem e Transformação da imagem\n");
        int op = scan.nextInt();
        scan.nextLine();
        return op;
    }

    public static int menuEstatísticas() {
        System.out.print("Escolha uma opção:\n"
                + "1 - Soma e média de cada uma das linhas\n"
                + "2 - Soma e média de cada uma das colunas\n"
                + "3 - Soma e média de todos os elementos da matriz\n"
                + "4 - Conjunto de valores e vetores próprios\n");
        int op1 = scan.nextInt();
        scan.nextLine();
        return op1;
    }

    public static int menuTransformações() {
        System.out.print("Escolha uma opção:\n"
                + "1 - Média\n"
                + "2 - Mediana\n"
                + "3 - Mínimo\n"
                + "4 - Máximo\n"
                + "5 - Rotação\n");
        int op2 = scan.nextInt();
        scan.nextLine();
        return op2;
    }

    public static void estatísticasColunas(int[][] matriz, int size) {
        int somaColuna = 0;
        float mediaColuna = 0;
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                somaColuna += matriz[j][i];
                mediaColuna = somaColuna / size;
            }
            System.out.println("Soma da coluna " + i + ": " + somaColuna);
            System.out.println("Média da coluna " + i + ": " + mediaColuna);
            somaColuna = 0;
            mediaColuna = 0;
        }
    }

    public static void estatísticasLinhas(int[][] matriz, int size) {
        int somaLinha = 0;
        float mediaLinha = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                somaLinha += matriz[i][j];
                mediaLinha = somaLinha / size;
            }
            System.out.println("Soma da linha " + i + ": " + somaLinha);
            System.out.println("Média da linha " + i + ": " + mediaLinha);
            somaLinha = 0;
            mediaLinha = 0;
        }
    }
    
    public static void estatísticasMatriz(int[][] matriz, int size) {
        int soma = 0;
        float media = 0;
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz.length; j++){
                soma += matriz[i][j];
                media= soma / size * size;
            }
        }
        System.out.println("Soma de todos os elementos da matriz: " +soma);
        System.out.println("Média de todos os elementos da matriz: " +media);
    }

    private static void listarMatriz(int[][] matriz) {
        System.out.println("Listagem da matriz");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static String lerFicheiro() throws FileNotFoundException {
        boolean matrizPossivel = false;
        String linha;
        String nomeFich;
        do {
            System.out.println("Introduza o nome do ficheiro.");
            nomeFich = scan.nextLine() + ".txt";
            nomeFich = checkIfFileExist(nomeFich);
            Scanner fInput = new Scanner(new File(nomeFich));
            linha = fInput.nextLine();
            String[] título = linha.split("  ");
            int size = Integer.parseInt(título[1]);
            matrizPossivel = verificarMatriz(nomeFich, size);
        } while (matrizPossivel == false);
        linha = linha + "  " + nomeFich;
        return linha;
    }

    public static String checkIfFileExist(String nomeFich) {
        File f = new File(nomeFich);
        while (!f.exists() || f.isDirectory()) {
            System.out.format("%s%n%s%n", "Ficheiro não encontrado!",
                    "Introduza o nome do ficheiro que procura sem a sua extensão.");
            nomeFich = scan.nextLine() + ".txt";
            f = new File(nomeFich);
        }
        return nomeFich;
    }

    public static boolean verificarMatriz(String nomeFich, int size) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        fInput.nextLine();
        fInput.nextLine();
        int linhas = 0;
        while (fInput.hasNextLine()) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            linhas++;
            String[] linha1 = linha.split(",");
            if (linha1.length != size) {
                System.out.format("%s%n", "Matriz inválida1");
                return false;
            } else {
                for (int i = 0; i < size; i++) {
                    if (linha1[i].matches("[0-9]+")) {
                        int num = Integer.parseInt(linha1[i]);
                        if (num < LIM_MÍNIMO || num > LIM_MÁXIMO) {
                            System.out.format("%s%n", "Matriz inválida2");
                            return false;
                        }
                    } else {
                        System.out.format("%s%n", "Matriz inválida3");
                        return false;
                    }
                }
            }
        }
        if (linhas != size) {
            System.out.format("%s%n", "Matriz inválida4");
            return false;
        }
        return true;
    }

    public static void lerMatriz(String nomeFich, int size, int[][] matriz) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        fInput.nextLine();
        fInput.nextLine();
        for (int i = 0; i < size; i++) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            String[] temp = linha.split(",");
            for (int j = 0; j < size; j++) {
                matriz[i][j] = Integer.parseInt(temp[j]);
            }
        }
    }
}
