
package lapr1projetofinal;


public class Constantes {
    public final static int LIM_MÍNIMO = 0;
    public final static int LIM_MÁXIMO = 255;

    public final static String ESPAÇAMENTO_TÍTULO = "  ";
    public final static String ESPAÇAMENTO_DECOMPOSIÇAO = ",";
    public final static String SEPARADOR_MATRIZ = ",";
    public final static String REGEX_TÍTULO_TXT = "^[#]\\S+(\\s\\S+)*[ ][ ][0-9]+[ ]*$";
    /**
     * Ao mudar o ESPAÇAMENTO_TÍTULO terá de mudar também o REGEX_TÍTULO_TXT, substituindo a parte "[ ][ ]" pelo regex que
     * corresponde ao novo ESPAÇAMENTO_TÍTULO
     */
    
}
