package lapr1projetofinal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Formatter;
import java.util.Scanner;
import static lapr1projetofinal.Constantes.*;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.decomposition.SingularValueDecompositor;

public class LAPR1ProjetoFinal {

    public static Scanner scan = new Scanner(System.in);

    /**
     * O programa começa por invocar o metodo lerFicheiro Com isto guarda-se, se
     * possível, os parametros da primeira linha e testa-se o ficheiro em
     * questão até este ser válido Mostra-se o menu principal ao utilizador
     *
     * @param args parametro dado pelo utilizador
     * @throws FileNotFoundException
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        Path file = new File(args[0]).toPath();
        if (Files.isRegularFile(file)) {
            String tituloImag = lerFicheiro(args);
            String[] titulo = tituloImag.split(ESPAÇAMENTO_TÍTULO);
            String nome = titulo[0];
            int size = Integer.parseInt(titulo[1]);
            int[][] matriz = new int[size][size];
            String nomeFich = titulo[2];
            if (titulo.length == 4) {
                menuDescompressao(nomeFich, System.getProperty("user.dir"));
                nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf(".")) + "_DESCOM.txt";
            }
            lerMatriz(nomeFich, size, matriz);
            nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf("."));
            menuPrincipal(matriz, size, nomeFich, nome);
        }else{
            int i[][] = new int[1][1];
            menuVisualizador(i, "", "", args[0], false);
        }
    }

    /**
     * Deixa o utilizador ver os resultados antes de escolher nova opção
     *
     */
    private static void pausa() {
        System.out.println("\nPara continuar digite ENTER");
        scan.nextLine();
    }

    /**
     * Pede o nome do ficheiro ao utilizador. Enquanto que o ficheiro dado tiver
     * erros, estiver mal formatado ou não exita o programa vai pedir novo nome
     * de ficheiro.
     *
     * @param args parametro dado pelo utilizador
     * @return String com a primeira linha do ficheiro e o nome do ficheiro
     * @throws FileNotFoundException
     * @throws java.lang.InterruptedException
     */
    public static String lerFicheiro(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        String matrizPossivel;
        String matrizCompPossivel;
        String tituloImag;
        String nomeFich;
        nomeFich = args[0];
        boolean extDir;
        boolean extFil;
        do {
            do {
                extDir = checkIfDirExist(nomeFich);
                if (extDir == true) {
                    mostrarImagDir(nomeFich);
                }
                extFil = checkIfFileExist(nomeFich);
                if (extFil == false) {
                    System.out.format("%s%n%s%n", "Ficheiro não encontrado!", "Insira o nome novamente");
                    nomeFich = scan.nextLine();
                }
            } while (extFil == false);
            Scanner fInput = new Scanner(new File(nomeFich));
            tituloImag = fInput.nextLine().replaceAll("[ ][ ]+", ESPAÇAMENTO_TÍTULO).trim();
            String[] titulo = tituloImag.split(ESPAÇAMENTO_TÍTULO);
            titulo[1] = titulo[1].replaceAll("[^0-9]+", "1");
            int size = Integer.parseInt(titulo[1]);
            matrizPossivel = verificarMatriz(nomeFich, System.getProperty("user.dir"));
            matrizCompPossivel = verificarMatrizComp(nomeFich, System.getProperty("user.dir"));
            if (!matrizPossivel.matches("PASS") && !matrizCompPossivel.matches("[0-9]+")) {
                System.out.println(matrizPossivel + "/nPor favor insira o nome de um ficheiro/Directório válido");
                nomeFich = scan.nextLine();
            }
        } while (!matrizPossivel.matches("PASS") && !matrizCompPossivel.matches("[0-9]+"));
        tituloImag = tituloImag + ESPAÇAMENTO_TÍTULO + nomeFich;
        if (matrizCompPossivel.matches("[0-9]+")) {
            tituloImag = tituloImag + ESPAÇAMENTO_TÍTULO + matrizCompPossivel;
        }
        return tituloImag;
    }

    /**
     * Mostra com o gnuplot todas as imagens na pasta indicada e fecha o
     * programa se a pasta estiver vazia retorna um false
     *
     * @param nomeFich - String com o nome da Pasta
     * @return false se a pasta em questão estiver vazia
     * @throws IOException
     * @throws InterruptedException
     */
    public static boolean mostrarImagDir(String nomeFich) throws IOException, InterruptedException {
        String textFiles = procurarImagens(nomeFich);
        String[] imagens = textFiles.split("&");
        if (imagens.length > 1) {
            for (int i = 1; i < imagens.length; i++) {
                Runtime rt = Runtime.getRuntime();
                //rt.exec("cd " + nomeFich);
                rt.exec("gnuplot -e \"filename=\'" + imagens[i].trim() + "\'\"  script.gp");
                Thread.sleep(5000);
            }
            System.out.println("Muito Obrigado");
            System.exit(0);
        } else {
            System.out.println("A pasta indicada não tem nenhuma imagem válida");
        }
        return false;
    }

    /**
     * Testa se o nome dado pelo utilizador corresponde a um Diretório
     * existente.
     *
     * @param nomeFich - String com o nome da pasta
     * @return sim ou não
     */
    public static boolean checkIfDirExist(String nomeFich) {
        File f = new File(nomeFich);
        if (f.exists() && f.isDirectory()) {
            return true;
        }
        return false;
    }

    /**
     * Testa se o nome dado pelo utilizador corresponde a um ficheiro txt
     * existente.
     *
     * @param nomeFich - String com o nome do ficheiro
     * @return true ou false
     */
    public static boolean checkIfFileExist(String nomeFich) {
        File f = new File(nomeFich);
        if (f.exists() && !f.isDirectory() && nomeFich.endsWith(".txt")) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o ficheiro está bem formatado e retorna uma String dependendo
     * de A primeira linha só ter o nome da imagem As dimensões da matriz serem
     * separadas por dois espaços A segunda linha estar vazia A matriz ter as
     * dimensões dadas no título A matriz ser só composta por números de 0 a 255
     * Se passar todos os testes retorna a resposta PASS
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param directory - nome do diretório onde vai ser procurada a imagem
     * @return String com erros ou a mensagem PASS
     * @throws FileNotFoundException
     */
    public static String verificarMatriz(String nomeFich, String directory) throws FileNotFoundException, IOException {
        Scanner fInput = new Scanner(new File(directory + File.separator + nomeFich));
        String line0 = fInput.nextLine().trim();
        if (!line0.startsWith("#")) {
            return "Título inválido no ficheiro " + nomeFich
                    + "/nCertifique-se que o título da imagem começa por um #";
        }
        if (!line0.matches(REGEX_TÍTULO_TXT)) {
            return "Título inválido no ficheiro " + nomeFich;
        }
        String[] titulo = line0.split(ESPAÇAMENTO_TÍTULO);
        if (titulo.length != 2) {
            return "Título inválido no ficheiro " + nomeFich;
        }
        int size = Integer.parseInt(titulo[1]);
        String line1 = fInput.nextLine();
        if (!line1.trim().isEmpty()) {
            return nomeFich + " é um ficheiro inválido!";
        }
        int linhas = 0;
        while (fInput.hasNextLine()) {
            String linha = fInput.nextLine().replaceAll(" +", "");
            String[] linhaAR = linha.split(SEPARADOR_MATRIZ);
            if (linhaAR.length != size) {
                return "Matriz inválida no ficheiro" + nomeFich;
            } else {
                for (int i = 0; i < size; i++) {
                    if (linhaAR[i].matches("[0-9]+")) {
                        int num = Integer.parseInt(linhaAR[i]);
                        if (num < Constantes.LIM_MÍNIMO || num > Constantes.LIM_MÁXIMO) {
                            return "Matriz inválida no ficheiro" + nomeFich;
                        }
                    } else {
                        return "Matriz inválida no ficheiro" + nomeFich;
                    }
                }
            }
            linhas++;
        }
        if (linhas != size) {
            return "Matriz inválida no ficheiro" + nomeFich;
        }
        return "PASS";
    }

    /**
     * Verifica se o ficheiro em questão é uma imagem comprimida e dá uma
     * resposta dependendo do que falhou ou não nos testes feitos
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param directory - nome do diretório onde vai ser procurada a imagem
     * @return mensagem de erro ou o numero de valores singulares
     * @throws FileNotFoundException
     */
    public static String verificarMatrizComp(String nomeFich, String directory) throws FileNotFoundException {
        int cont = 0;
        Scanner fInput = new Scanner(new File(directory + File.separator + nomeFich));
        String linhaTeste0 = fInput.nextLine().trim();
        if (!linhaTeste0.startsWith("#")) {
            return "Título inválido no ficheiro " + nomeFich
                    + "/nCertifique-se que o título da imagem começa por um #";
        }
        if (!linhaTeste0.matches(REGEX_TÍTULO_TXT)) {
            return "Título inválido no ficheiro "
                    + nomeFich;
        }
        String[] titulo = linhaTeste0.split(ESPAÇAMENTO_TÍTULO);
        int size = Integer.parseInt(titulo[1]);
        String linhaTesteNull = fInput.nextLine();
        if (!linhaTesteNull.trim().isEmpty()) {
            return nomeFich + " é um ficheiro inválido!";
        }
        while (cont < size && fInput.hasNextLine()) {
            String linhaTesteVAL = fInput.nextLine();
            if (!linhaTesteVAL.matches("[0-9\\.]*")) {
                return "Valor singular inválido no ficheiro "
                        + nomeFich;
            }
            String linhaTeste1 = fInput.nextLine().replaceAll(" +", "");
            String[] linhaTeste1Ar = linhaTeste1.split(ESPAÇAMENTO_DECOMPOSIÇAO);
            if (linhaTeste1Ar.length != size) {
                return "Vetor inválido no ficheiro "
                        + nomeFich;
            }
            String linhaTeste2 = fInput.nextLine().replaceAll(" +", "");
            String[] linhaTeste2Ar = linhaTeste2.split(ESPAÇAMENTO_DECOMPOSIÇAO);
            if (linhaTeste2Ar.length != size) {
                return "Vetor inválido no ficheiro "
                        + nomeFich;
            }
            String linhaTeste3 = fInput.nextLine();
            if (!linhaTeste3.trim().isEmpty()) {
                return nomeFich + " é um ficheiro inválido!";
            }
            cont++;
        }
        while (fInput.hasNextLine()) {
            String linhaTesteFinal = fInput.nextLine();
            if (!linhaTesteFinal.trim().isEmpty()) {
                return nomeFich + " é um ficheiro inválido!";
            }
        }
        String contString = Integer.toString(cont);
        return contString;
    }

    /**
     * Guarda a matriz dada no ficheiro em memória, no parametro "matriz"
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param size - dimensões da matriz
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @throws FileNotFoundException
     */
    public static void lerMatriz(String nomeFich, int size, int[][] matriz) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        fInput.nextLine();
        fInput.nextLine();
        for (int i = 0; i < size; i++) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            String[] temp = linha.split(",");
            for (int j = 0; j < size; j++) {
                matriz[i][j] = Integer.parseInt(temp[j]);
            }
        }
    }

    /**
     * Menu principal, dergunta ao utilizador se quer ver as estatísticas da
     * matriz ou aplicar filtros O programa pára se escolhermos a opção 0 - Sair
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void menuPrincipal(int[][] matriz, int size, String nomeFich, String nome) throws IOException, InterruptedException {
        String op;
        int[][] matrizTransformada;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Estatísticas",
                    "2 - Filtros",
                    "3 - Ordenação",
                    "4 - Visualizar",
                    "5 - Comprimir Imagem Original",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    menuEstatistica(matriz, size);
                    break;
                case "2":
                    menuFiltros(matriz, size, nomeFich, nome);
                    break;
                case "3":
                    matrizTransformada = filtrosSort(matriz, size, nomeFich, nome);
                    listarMatriz(matrizTransformada);
                    break;
                case "4":
                    menuVisualizador(matriz, nome, nomeFich,System.getProperty("user.dir"),true);
                    break;
                case "5":
                    String testeNomeFich = nomeFich.replaceAll("_DESCOM", "");
                    if (!verificarMatrizComp(testeNomeFich + ".txt", System.getProperty("user.dir")).matches("[0-9]+")) {
                        compressaoImagem(size, matriz, nomeFich, nome);
                    } else {
                        System.out.println("A Imagem dada já está comprimida");
                    }
                    break;
                case "0":
                    System.out.format("%n%s%n%s%n", "Muito Obrigado!", "Fim");
                    System.exit(0);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Ordena os valores da matriz do maior ao mais pequeno Passando primeiro os
     * valores para um vector
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz com o filtro já aplicado
     * @throws IOException
     */
    public static int[][] filtrosSort(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[] vetorTotal = matrizParaVetor(matriz, size);
        int[] vetorOrde = doSelectionSort(vetorTotal);
        int[][] matrizTransformada = vetorParaMatriz(vetorOrde, size, matriz, nomeFich, nome);
        return matrizTransformada;
    }

    /**
     * Ordena os valores do vector matriz do maior para o menor
     *
     * @param matriz - vetor com todos os valores da matriz
     * @return vector de inteiros com todos os valores da matriz
     */
    public static int[] doSelectionSort(int[] matriz) {

        for (int i = 0; i < matriz.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < matriz.length; j++) {
                if (matriz[j] > matriz[index]) {
                    index = j;
                }
            }

            int smallerNumber = matriz[index];
            matriz[index] = matriz[i];
            matriz[i] = smallerNumber;
        }
        return matriz;
    }

    /**
     * Guarda os valores da matriz num vector de inteiros
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @return vector de inteiros com todos os valores da matriz
     */
    public static int[] matrizParaVetor(int matriz[][], int size) {
        int[] vetor = new int[size * size];
        int cont = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                vetor[cont] = matriz[i][j];
                cont = cont + 1;
            }
        }
        return vetor;
    }

    /**
     * Passa os valores por ordem para uma nova matriz e mostra a ao utilizador
     *
     * @param vetor - vetor com todos os valores da matriz ordenados
     * @param size - dimensões da matriz
     * @param matriz - matriz alterada pelos filtros
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz com o filtro já aplicado
     * @throws IOException
     */
    public static int[][] vetorParaMatriz(int[] vetor, int size, int[][] matriz, String nomeFich, String nome) throws IOException {
        int[][] matrizTransformada = new int[size][size];
        int contador = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizTransformada[i][j] = vetor[contador];
                contador++;
            }
        }
        return matrizTransformada;
    }

    /**
     * Menu dos calculos estatisticos Voltamos ao menu principal ao escolher a
     * opção 0
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void menuEstatistica(int[][] matriz, int size) {
        String op;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Soma e média de cada uma das linhas",
                    "2 - Soma e média de cada uma das colunas",
                    "3 - Soma e média de todos os elementos da matriz",
                    "4 - Conjunto de valores e vetores próprios",
                    "5 - Regra dos trapézios simples",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    estatisticasLinhas(matriz, size);
                    pausa();
                    break;
                case "2":
                    estatisticasColunas(matriz, size);
                    pausa();
                    break;
                case "3":
                    int soma = estatisticasMatriz(matriz, size);
                    float media = (float) soma / (size * size);
                    System.out.format("%s%d%n", "Soma de todos os elementos da matriz: ", soma);
                    System.out.format("%s%.2f%n", "Média de todos os elementos da matriz: ", media);
                    pausa();
                    break;
                case "4":
                    double[][] matrizDouble = new double[size][size];
                    vetorValorProprio(matrizDouble, matriz, size);
                    pausa();
                    break;
                case "5":
                    estatisticasTrapezio(matriz, size);
                    pausa();
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Calcula a média e soma de cada coluna Mostra dois valores devidamente
     * carcterizados para cada coluna da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void estatisticasColunas(int[][] matriz, int size) {
        int somaColuna = 0;
        float mediaColuna;
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                somaColuna += matriz[j][i];
            }
            mediaColuna = (float) somaColuna / size;
            System.out.format("%s%d%s%d%n", "Soma da coluna ", i + 1, ": ", somaColuna);
            System.out.format("%s%d%s%.2f%n", "Média da coluna ", i + 1, ": ", mediaColuna);
            somaColuna = 0;
        }
    }

    /**
     * Calcula a média e soma de cada linha Mostra dois valores devidamente
     * carcterizados para cada linha da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void estatisticasLinhas(int[][] matriz, int size) {
        int somaLinha = 0;
        float mediaLinha;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                somaLinha += matriz[i][j];
            }
            mediaLinha = (float) somaLinha / size;
            System.out.format("%s%d%s%d%n", "Soma da linha ", i + 1, ": ", somaLinha);
            System.out.format("%s%d%s%.2f%n", "Média da linha ", i + 1, ": ", mediaLinha);
            somaLinha = 0;
        }
    }

    /**
     * Calcula e retorna a soma dos elementos da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @return soma dos elementos da matriz
     */
    public static int estatisticasMatriz(int[][] matriz, int size) {
        int soma = 0;
        float media = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                soma += matriz[i][j];
            }
        }
        return soma;
    }

    /**
     * Criar objeto do tipo Matrix com a linguagem da livraria Obtem valores e
     * vetores próprios fazendo "Eigen Decomposition" converte objeto Matrix
     * (duas matrizes) para array Java
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @param matrizDouble - matriz criada para converter a matriz de inteiros
     * em double
     */
    public static void vetorValorProprio(double[][] matrizDouble, int[][] matriz, int size) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizDouble[i][j] = (double) matriz[i][j];
            }
        }
        Matrix a = new Basic2DMatrix(matrizDouble);
        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] Decompose = eigenD.decompose();
        double matB[][] = Decompose[1].toDenseMatrix().toArray();
        System.out.format("%s%n", "Valores próprios:");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matB[i][j] != 0) {
                    System.out.format("%.2f%n", matB[i][j]);
                }
            }
        }
        System.out.format("%n");
        double matA[][] = Decompose[0].toDenseMatrix().toArray();
        System.out.format("%s%n", "Vetores próprios:");
        for (int i = 0; i < matriz.length; i++) {
            System.out.format("%s", "{(");
            for (int j = 0; j < matriz.length - 1; j++) {
                System.out.format("%.2f%s", matA[j][i], ",");
            }
            System.out.format("%.2f", matA[matriz.length - 1][i]);
            System.out.format("%s%n", ")}");
        }
        System.out.format("%n");
    }

    /**
     * Calcula, através da regra dos trapézios simples, um valor para cada linha
     * e retorna esse valor
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void estatisticasTrapezio(int[][] matriz, int size) {
        int soma = 0;
        float trapezioLinha;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                soma = soma + matriz[i][j];
            }
            for (int l = 1; l < size - 1; l++) {
                soma = soma + matriz[i][l];
            }
            trapezioLinha = (float) soma / 2;
            System.out.format("%s%d%s%.1f%n", "Soma da linha ", i + 1, ": ", trapezioLinha);
        }
    }

    /**
     * Menu da escolha de filtros Voltamos ao menu principal ao escolher a opção
     * 0
     *
     * @param mat - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void menuFiltros(int[][] mat, int size, String nomeFich, String nome) throws IOException, InterruptedException {
        String op;
        int[][] matAlterada = new int[size][size];
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Média",
                    "2 - Mediana",
                    "3 - Máximo",
                    "4 - Mínimo",
                    "5 - Rotação",
                    "6 - Convolução",
                    "7 - Variância",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Media");
                    break;
                case "2":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Mediana");
                    break;
                case "3":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Maximo");
                    break;
                case "4":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Minimo");
                    break;
                case "5":
                    menuRotacao(mat, matAlterada, size, nomeFich, nome);
                    break;
                case "6":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Convolução");
                    break;
                case "7":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Variância");
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuPrincipal(mat, size, nomeFich, nome);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Aplica o filtro escolhido no menu à matriz guardada, pixel a pixel, como
     * pedido (3x3) Mostra a matriz alterada ao utilizador Grava as alterações
     * em memória na nova matriz matAlterada
     *
     * @param mat - matriz guardada em memória
     * @param matAlterada - matriz alterada pelos filtros
     * @param size - dimensões da matriz
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @param nomeFich - nome do ficheiro em questão
     * @param op - opção escolhida pelo utilizador no menu dos filtros
     * @param message - tipo de filtro utilizado ( relacionado com a opção
     * escolhida)
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void aplicarFiltros(int[][] mat, int[][] matAlterada, int size, String nome, String nomeFich, String op, String message) throws IOException, InterruptedException {
        int[] vetorKernel = new int[9];
        float[] vetorKernelNorm = new float[9];
        if (op.equals("6")) {
            vetorKernelNorm = pedirMatrizKernel(vetorKernel);
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int vetorValores[] = guardarVetorValores(mat, size, i, j);
                switch (op) {
                    case "1":
                        matAlterada[i][j] = filtroMedia(vetorValores);
                        break;
                    case "2":
                        matAlterada[i][j] = filtroMediana(vetorValores);
                        break;
                    case "3":
                        matAlterada[i][j] = filtroMaximo(vetorValores);
                        break;
                    case "4":
                        matAlterada[i][j] = filtroMinimo(vetorValores);
                        break;
                    case "6":
                        matAlterada[i][j] = filtroConvolucao(vetorValores, vetorKernelNorm);
                        break;
                    case "7":
                        matAlterada[i][j] = filtroVariancia(vetorValores);
                        break;
                }
            }
        }
        listarMatriz(matAlterada);
        nomeFich = (nomeFich + "_" + message);
        menuFiltrosOp(mat, matAlterada, size, nomeFich, nome, message);
    }

    /**
     * Apresenta um novo menu ao utilizador com as opções: -Criar novo ficheiro
     * com a matriz alterada pelo filtros escolhido -Apaga a matriz alterada e
     * volta a pedir ao utilizador um filtro para aplicar na matriz inalterada
     * pelo ultimo filtro -Aplica um novo filtro à matriz já alterada -Apaga a
     * matriz alterada e volta ao menu principal
     *
     * @param matriz - matriz original guardada em memória
     * @param matrizAlterada - matriz alterada pelos filtros
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @param message - tipo de filtro utilizado ( relacionado com a opção
     * escolhida)
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void menuFiltrosOp(int[][] matriz, int[][] matrizAlterada, int size, String nomeFich, String nome, String message) throws IOException, InterruptedException {
        String op;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n", "Pretende:",
                    "1- Guardar o filtro",
                    "2- Comprimir imagem",
                    "3- Escolher um filtro diferente",
                    "4- Escolher mais filtros para aplicar",
                    "0- Sair sem gravar alterações");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    guardarNovoFicheiro(matrizAlterada, size, nomeFich, nome, System.getProperty("user.dir"));
                    System.out.format("%n%s%n%s%n", "Alterações gravadas", "Obrigado!");
                    break;
                case "2":
                    compressaoImagem(size, matriz, nomeFich, nome);
                    pausa();
                    break;
                case "3":
                    nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf("_"));
                    menuFiltros(matriz, size, nomeFich, nome);
                    break;
                case "4":
                    menuFiltros(matrizAlterada, size, nomeFich, nome);
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuPrincipal(matriz, size, nomeFich, nome);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Retira da matriz dada 9 valores que correspondem ao pixel na posição
     * (i,j) e os 8 valores que o rodeiam No caso de o pixel estar na
     * "fronteira" da matriz os valores que não existem são criados com base nos
     * valores disponíveis Retorna esses 9 valores guardados num vector
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param i - linha do pixel a alterar
     * @param j - coluna do pixel a alterar
     * @return vetor com os 9 valores em questão
     */
    public static int[] guardarVetorValores(int[][] matriz, int size, int i, int j) {
        int[] vetorValores = new int[9];
        int cont = 0;
        if (i == 0) {
            if (j == 0) {
                vetorValores[0] = 0;
                vetorValores[1] = matriz[i][j];
                vetorValores[2] = matriz[i][j + 1];
                vetorValores[3] = matriz[i][j];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j + 1];
                vetorValores[6] = matriz[i + 1][j];
                vetorValores[7] = matriz[i + 1][j];
                vetorValores[8] = matriz[i + 1][j + 1];
            } else if (j == size - 1) {
                vetorValores[0] = matriz[i][j - 1];
                vetorValores[1] = matriz[i][j];
                vetorValores[2] = 0;
                vetorValores[3] = matriz[i][j - 1];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j];
                vetorValores[6] = matriz[i + 1][j - 1];
                vetorValores[7] = matriz[i + 1][j];
                vetorValores[8] = matriz[i + 1][j];
            } else {
                vetorValores[0] = matriz[i][j - 1];
                vetorValores[1] = matriz[i][j];
                vetorValores[2] = matriz[i][j + 1];
                vetorValores[3] = matriz[i][j - 1];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j + 1];
                vetorValores[6] = matriz[i + 1][j - 1];
                vetorValores[7] = matriz[i + 1][j];
                vetorValores[8] = matriz[i + 1][j + 1];
            }
        } else if (i == size - 1) {
            if (j == 0) {
                vetorValores[0] = matriz[i - 1][j];
                vetorValores[1] = matriz[i - 1][j];
                vetorValores[2] = matriz[i][j + 1];
                vetorValores[3] = matriz[i][j];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j + 1];
                vetorValores[6] = 0;
                vetorValores[7] = matriz[i][j];
                vetorValores[8] = matriz[i][j + 1];
            } else if (j == size - 1) {
                vetorValores[0] = matriz[i - 1][j - 1];
                vetorValores[1] = matriz[i - 1][j];
                vetorValores[2] = matriz[i - 1][j];
                vetorValores[3] = matriz[i][j - 1];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j];
                vetorValores[6] = matriz[i][j - 1];
                vetorValores[7] = matriz[i][j];
                vetorValores[8] = 0;
            } else {
                vetorValores[0] = matriz[i - 1][j - 1];
                vetorValores[1] = matriz[i - 1][j];
                vetorValores[2] = matriz[i - 1][j + 1];
                vetorValores[3] = matriz[i][j - 1];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j + 1];
                vetorValores[6] = matriz[i][j - 1];
                vetorValores[7] = matriz[i][j];
                vetorValores[8] = matriz[i][j + 1];
            }
        } else {
            if (j == 0) {
                vetorValores[0] = matriz[i - 1][j];
                vetorValores[1] = matriz[i - 1][j];
                vetorValores[2] = matriz[i - 1][j + 1];
                vetorValores[3] = matriz[i][j];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j + 1];
                vetorValores[6] = matriz[i + 1][j];
                vetorValores[7] = matriz[i + 1][j];
                vetorValores[8] = matriz[i + 1][j + 1];
            } else if (j == size - 1) {
                vetorValores[0] = matriz[i - 1][j - 1];
                vetorValores[1] = matriz[i - 1][j];
                vetorValores[2] = matriz[i - 1][j];
                vetorValores[3] = matriz[i][j - 1];
                vetorValores[4] = matriz[i][j];
                vetorValores[5] = matriz[i][j];
                vetorValores[6] = matriz[i + 1][j - 1];
                vetorValores[7] = matriz[i + 1][j];
                vetorValores[8] = matriz[i + 1][j];
            } else {
                for (int i2 = -1; i2 <= 1; i2++) {
                    for (int j2 = -1; j2 <= 1; j2++) {
                        vetorValores[cont] = matriz[i + i2][j + j2];
                        cont = cont + 1;
                    }
                }
            }
        }
        return vetorValores;
    }

    /**
     * Faz a média dos 9 valores dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMedia(int[] vetorValores) {
        int soma = 0;
        for (int i = 0; i < vetorValores.length; i++) {
            soma = soma + vetorValores[i];
        }
        int media = soma / vetorValores.length;
        return media;
    }

    /**
     * Faz a mediana dos 9 valores dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMediana(int[] vetorValores) {
        for (int i = 0; i < vetorValores.length; i++) {
            for (int j = i + 1; j < vetorValores.length; j++) {
                int tmp;
                if (vetorValores[i] > vetorValores[j]) {
                    tmp = vetorValores[i];
                    vetorValores[i] = vetorValores[j];
                    vetorValores[j] = tmp;
                }
            }
        }
        return vetorValores[4];
    }

    /**
     * Procura o valor mais alto dos 9 dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMaximo(int[] vetorValores) {
        int valorMaximo = -1;
        for (int i = 0; i < vetorValores.length; i++) {
            if (valorMaximo < vetorValores[i]) {
                valorMaximo = vetorValores[i];
            }
        }
        return valorMaximo;
    }

    /**
     * Procura o valor mais baixo dos 9 dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMinimo(int[] vetorValores) {
        int valorMinimo = 256;
        for (int i = 0; i < vetorValores.length; i++) {
            if (valorMinimo > vetorValores[i]) {
                valorMinimo = vetorValores[i];
            }
        }
        return valorMinimo;
    }

    /**
     * Apresenta um menu que deixa o utilizador escolher se quer rodar a matriz
     * 90º para a direita ou esquerda
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @param matAlterada - matriz alterada pelos filtros
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public static void menuRotacao(int[][] matriz, int[][] matAlterada, int size, String nomeFich, String nome) throws IOException, InterruptedException {
        String op;
        String message = null;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Rotação no sentido horário",
                    "2 - Rotação no sentido anti-horário",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    int[][] matrizRodadaH = filtroRotacaoHoraria(matriz, size, nomeFich, nome);
                    listarMatriz(matrizRodadaH);
                    message = "RotacaoSentidoHorario";
                    nomeFich = (nomeFich + "_" + message);
                    menuFiltrosOp(matriz, matrizRodadaH, size, nomeFich, nome, message);
                    break;
                case "2":
                    int[][] matrizRodadaA = filtroRotacaoAntiH(matriz, size, nomeFich, nome);
                    listarMatriz(matrizRodadaA);
                    message = "RotacaoSentidoAnti-Horario";
                    nomeFich = (nomeFich + "_" + message);
                    menuFiltrosOp(matriz, matrizRodadaA, size, nomeFich, nome, message);
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuFiltrosOp(matriz, matriz, size, nomeFich, nome, message);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Roda a matriz dada para a direita
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz original rodada para a direita
     * @throws IOException
     */
    public static int[][] filtroRotacaoHoraria(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[][] matrizRodadaH = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizRodadaH[i][j] = matriz[size - j - 1][i];
            }
        }
        return matrizRodadaH;
    }

    /**
     * Roda a matriz dada para a esquerda
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz original rodada para a esquerda
     * @throws IOException
     */
    public static int[][] filtroRotacaoAntiH(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[][] matrizRodadaA = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizRodadaA[i][j] = matriz[j][size - i - 1];
            }
        }
        return matrizRodadaA;
    }

    /**
     * Pede ao utilizador a matriz a aplicar com o filtro convolução, pede nova
     * informação até esta ser um inteiro Normaliza a matriz dada
     *
     * @param vetorKernel parametro onde vão ser guardados os valores dados
     * @return vetor com valores dados normalizados
     */
    public static float[] pedirMatrizKernel(int[] vetorKernel) {
        int k = -1;
        int soma = 0;
        float vetorKernelNorm[] = new float[9];
        System.out.format("%s%n", "Qual a matriz Kernel a aplicar?");
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                System.out.format("%s%d%s%d%s%n", "Valor da posição [", i, ",", j, "]");
                String resposta = scan.nextLine();
                while (!resposta.matches("[-]?[0-9]+")) {
                    System.out.format("%s%n", "Por favor insira um valor inteiro");
                    resposta = scan.nextLine();
                }
                k++;
                int intResposta = Integer.parseInt(resposta);
                soma = soma + intResposta;
                vetorKernel[k] = intResposta;
            }
        }
        for (int l = 0; l < vetorKernel.length; l++) {
            vetorKernelNorm[l] = (float) vetorKernel[l] / 9;
        }
        return vetorKernelNorm;
    }

    /**
     * Multiplica cada elemento dado pelo valor correspondente na matriz e soma
     * os valores obtidos, se o valor final não estiver entre os valores 0 e 255
     * é substituido por um desses
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * @param vetorKernelNorm - vetor com os 9 valores dados pelo utilizador, já
     * normalizados
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroConvolucao(int[] vetorValores, float[] vetorKernelNorm) {
        int valorConvolucao = 0;
        for (int i = 0; i < vetorValores.length; i++) {
            valorConvolucao = Math.round(valorConvolucao + vetorValores[i] * vetorKernelNorm[i]);
        }
        if (valorConvolucao > LIM_MÁXIMO) {
            valorConvolucao = LIM_MÁXIMO;
        }
        if (valorConvolucao < LIM_MÍNIMO) {
            valorConvolucao = LIM_MÍNIMO;
        }
        return valorConvolucao;
    }

    /**
     * Calcula a média e aplica a fórmula da variãncia ao pixel em questão
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroVariancia(int vetorValores[]) {
        int media = filtroMedia(vetorValores);
        int soma = 0;
        for (int i = 0; i < vetorValores.length; i++) {
            soma = soma + (int) (Math.pow(vetorValores[i] - media, 2));
        }
        float vari = (float) soma / 9;
        if (vari > LIM_MÁXIMO) {
            return LIM_MÁXIMO;
        }
        int variInt = Math.round(vari);
        return variInt;
    }

    /**
     * Mostra ao utilizador a matriz em questão
     *
     * @param matriz - matriz que vai ser mostrada ao utilizador
     */
    public static void listarMatriz(int[][] matriz) {
        System.out.format("%n%s%n", "Listagem da matriz");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.format("%3s%s", matriz[i][j], " ");
            }
            System.out.println();
        }
    }

    /**
     * Guarda num novo ficheiro a matriz alterada pelos filtros utilizados pelo
     * utilizador. Dá ao ficheiro criado o nome do ficheiro original + os
     * filtros utilizados
     *
     * @param matriz - matriz em questão
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @param directory - nome do directório em que o ficheiro vai ser guardado
     * @throws IOException
     */
    public static void guardarNovoFicheiro(int[][] matriz, int size, String nomeFich, String nome, String directory) throws IOException {
        Formatter out = new Formatter(new File(directory + File.separator + nomeFich + ".txt"));
        out.format("%s", nome + ESPAÇAMENTO_TÍTULO + size);
        out.format("%s%n%n", "");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                out.format("%s", matriz[i][j]);
                if (j != size - 1) {
                    out.format("%s", SEPARADOR_MATRIZ);
                }
            }
            if (i != size - 1) {
                out.format("%n%s", "");
            }
        }
        out.close();
    }

    /**
     * Cria um objeto do tipo Matriz, converte um objeto Matrix (duas matrizes)
     * para um array Java e guarda os resultados num ficheiro
     *
     * @param size - dimensões da matriz
     * @param matriz - matriz em questão
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws FileNotFoundException
     */
    public static void compressaoImagem(int size, int[][] matriz, String nomeFich, String nome) throws FileNotFoundException {
        double[][] matriz2 = new double[size][size];
        int valores;
        for (int j = 0; j < size; j++) {
            for (int i = 0; i < size; i++) {
                matriz2[i][j] = (double) matriz[i][j];
            }
        }
        do {
            System.out.format("%s%n%s%d%n", "Quantos valores deseja?", "*por favor escolha um número de 1 a ", size);
            valores = scan.nextInt();
            scan.nextLine();
        } while (valores > size || valores < 0);
        // Criar objeto do tipo Matriz
        Matrix a = new Basic2DMatrix(matriz2);
        // SVD decomposition
        SingularValueDecompositor eigenD = new SingularValueDecompositor(a);
        Matrix[] mattD = eigenD.decompose();
        // converte objeto Matrix (duas matrizes)  para array Java        
        double matU[][] = mattD[0].toDenseMatrix().toArray();
        //Matriz diagonal
        double matD[][] = mattD[1].toDenseMatrix().toArray();
        double matV[][] = mattD[2].toDenseMatrix().toArray();
        System.out.format("%n");
        boolean resposta = contarbytes(matU, matD, matV, valores, size);
        if (resposta == true) {
            Formatter out = new Formatter(new File(nomeFich + "_" + valores + "COMP.txt"));
            out.format("%s", nome + ESPAÇAMENTO_TÍTULO + size);
            out.format("%s%n%n", "");
            for (int i = 0; i < valores; i++) {
                out.format("%.2f%n", matD[i][i]);
                //Print column de matriz U
                for (int c = 0; c < size; c++) {
                    out.format("%.2f", matU[c][i]);
                    if (size != c + 1) {
                        out.format("%s", ESPAÇAMENTO_DECOMPOSIÇAO);
                    }
                }
                out.format("%n");
                //Print column de matriz V
                for (int e = 0; e < size; e++) {
                    out.format("%.2f", matV[e][i]);
                    if (size != e + 1) {
                        out.format("%s", ESPAÇAMENTO_DECOMPOSIÇAO);
                    }
                }
                out.format("%n%n");
            }
            out.close();
        }
    }

    /**
     * Guarda os valores da imagem comprimida em memória
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param directory - nome do diretório onde vai ser procurada a imagem
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void menuDescompressao(String nomeFich, String directory) throws FileNotFoundException, IOException {
        int size = 1;
        String valorString = verificarMatrizComp(nomeFich, directory);
        int valor = Integer.parseInt(valorString);
        Scanner fInput = new Scanner(new File(nomeFich));
        String line0 = fInput.nextLine();
        String[] titulo = line0.split(ESPAÇAMENTO_TÍTULO);
        size = Integer.parseInt(titulo[1]);
        fInput.nextLine();
        double[][] matD = new double[valor][valor];
        double[][] matU = new double[size][valor];
        double[][] matV = new double[size][valor];
        for (int i = 0; i < valor; i++) {
            matD[i][i] = Float.parseFloat(fInput.nextLine());
            String line2 = fInput.nextLine();
            String[] matValoresU = line2.split(ESPAÇAMENTO_DECOMPOSIÇAO);
            String line3 = fInput.nextLine();
            String[] matValoresV = line3.split(ESPAÇAMENTO_DECOMPOSIÇAO);
            for (int j = 0; j < size; j++) {
                matU[j][i] = Float.parseFloat(matValoresU[j]);
                matV[j][i] = Float.parseFloat(matValoresV[j]);
            }
            fInput.nextLine();
        }
        int[][] matrizDescomprimida = descompressao(matU, matD, matV, valor, size);
        nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf(".")) + "_DESCOM";
        guardarNovoFicheiro(matrizDescomprimida, size, nomeFich, titulo[0], directory);
    }

    /**
     * Utilizando os valores dados descomprime a matriz em questão
     *
     * @param matU - matriz com valores de todos os vetores U da matriz
     * @param matD - matriz com valores de todos os vetores D da matriz
     * @param matV - matriz com valores de todos os vetores V da matriz
     * @param valores - valores singulares da matriz
     * @param size - dimensões da matriz
     * @return - matriz descomprimida
     * @throws FileNotFoundException
     */
    public static int[][] descompressao(double[][] matU, double[][] matD, double[][] matV, int valores, int size) throws FileNotFoundException {
        double[][] matrizDescomprimidaF = new double[size][size];
        int[][] matrizDescomprimidaFInt = new int[size][size];
        for (int c = 0; c < valores; c++) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (valores == 1) {
                        matrizDescomprimidaF[i][j] = matU[i][0] * matV[j][0] * matD[c][c];
                    } else {
                        matrizDescomprimidaF[i][j] = matrizDescomprimidaF[i][j] + (matU[i][c] * matV[j][c] * matD[c][c]);
                    }
                }
            }
        }
        for (int i = 0; i < matrizDescomprimidaF.length; i++) {
            for (int j = 0; j < matrizDescomprimidaF[i].length; j++) {
                matrizDescomprimidaFInt[i][j] = (int) Math.round(matrizDescomprimidaF[i][j]);
                if (matrizDescomprimidaFInt[i][j] > LIM_MÁXIMO) {
                    matrizDescomprimidaFInt[i][j] = LIM_MÁXIMO;
                }
                if (matrizDescomprimidaFInt[i][j] < LIM_MÍNIMO) {
                    matrizDescomprimidaFInt[i][j] = LIM_MÍNIMO;
                }
            }
        }
        return matrizDescomprimidaFInt;
    }

    /**
     * Conta o tamanho em bytes da matriz original e da matriz comprimida e
     * compara os Mostra os resultados ao utilizador
     *
     * @param matU - matriz com valores de todos os vetores U da matriz
     * @param matD - matriz com valores de todos os vetores D da matriz
     * @param matV - matriz com valores de todos os vetores V da matriz
     * @param valores - valores singulares da matriz
     * @param size - dimensões da matriz
     * @return decisão sobre guardar ou não a imagem comprimida
     */
    public static boolean contarbytes(double[][] matU, double[][] matD, double[][] matV, int valores, int size) {
        int bytesMatrizInicial = 0;
        int bytesMatrizComprimida = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                bytesMatrizInicial++;
            }
        }
        for (int j = 0; j < valores; j++) {
            for (int l = 0; l < size; l++) {
                if (matD[j][l] != 0) {
                    bytesMatrizComprimida += 2;
                }
            }
            for (int m = 0; m < 6; m++) {
                bytesMatrizComprimida += 4;
            }
        }
        int spaceFreed = bytesMatrizInicial - bytesMatrizComprimida;
        System.out.format("%s%n%s%n%n", "A Imagem Inicial ocupa " + bytesMatrizInicial + " bytes!", "A Imagem Comprimida ocupa " + bytesMatrizComprimida + " bytes!");
        if (spaceFreed > 0) {
            System.out.format("%s%n", "Na compressão efetuada irá poupar " + spaceFreed + " bytes!");
        } else {
            spaceFreed = spaceFreed * (-1);
            int loop = 0;
            System.out.format("%s%n%s%n", "A imagem comprimida ocupa mais " + spaceFreed + " bytes que a imagem Inicial!", "Deseja mesmo assim comprimir a imagem?");
            String resposta = scan.nextLine();
            while (loop == 0) {
                if (resposta.equalsIgnoreCase("Y") || resposta.equalsIgnoreCase("sim") || resposta.equalsIgnoreCase("yes")) {
                    System.out.println("A imagem comprimida foi guardada com sucesso");
                    return true;
                } else if (resposta.equalsIgnoreCase("N") || resposta.equalsIgnoreCase("não") || resposta.equalsIgnoreCase("no") || resposta.equalsIgnoreCase("nao")) {
                    return false;
                } else {
                    System.out.format("%s%n", "Por favor responda com sim ou não");
                    resposta = scan.nextLine();
                }
            }
        }
        return true;
    }

    /**
     * Mostra na consola um menu onde o utilizador pode escolher entre sair, ver
     * todas as imagens ou escolher uma em especial
     *
     * @param matriz - matriz em questão
     * @param nome - título da imagem em questão
     * @param nomeFich - nome do ficheiro em questão
     * @throws InterruptedException
     * @throws IOException
     */
    public static void menuVisualizador(int[][] matriz, String nome, String nomeFich,String directory,boolean isFile) throws InterruptedException, IOException {
        int op;
        System.out.format("%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                "1 - Ver Todasas Imagens",
                "2 - Escolher uma Imagem",
                "0 - Sair");
        String resposta = scan.nextLine();
        while (!resposta.matches("[0-2]")) {
            System.out.format("%s%n", "Insira um número de 0 a 2");
            resposta = scan.nextLine();
        }
        op = Integer.parseInt(resposta);
        visualizador(matriz, nome, nomeFich, op,directory,isFile);
    }

    /**
     * Dependendo da opção escolhida vai: -sair para o menu principal -listar
     * todas as imagens que podem ser visualizadas e dar a escolher ao
     * utilizador uma delas para ser visualizada -mostrar todas as imagens
     * através do gnuplot
     *
     * @param matriz - matriz em questão
     * @param nome - título da imagem em questão
     * @param nomeFich - nome do ficheiro em questão
     * @param op - opção escolhida no menuVisualizador
     * @throws InterruptedException
     * @throws IOException
     */
    public static void visualizador(int[][] matriz, String nome, String nomeFich, int op,String directory,boolean isFile) throws InterruptedException, IOException {
        File dir = new File(directory);
        int size = matriz.length;
        int opcao = -1;
        if (op == 0) {
          if(isFile==true){
               menuPrincipal(matriz, size, nomeFich, nome);
            }else{
                System.exit(0);
            }
        }
        String listaImagens = procurarImagens(directory);
        String[] imagens = listaImagens.split("&");
        if (op == 1) {
            for (int i = 1; i < imagens.length; i++) {
                Runtime rt = Runtime.getRuntime();
                rt.exec("gnuplot -e \"filename=\'" + imagens[i].trim() + "\'\"  script.gp");
                Thread.sleep(5000);
            }
            menuVisualizador(matriz, nome, nomeFich,directory,isFile);
        } else if (op == 2) {
            System.out.println(imagens[0]);
            for (int i = 1; i < imagens.length; i++) {
                if (imagens[i].endsWith("_DESCOM.txt") && !verificarMatrizComp(imagens[i], System.getProperty("user.dir")).matches("[0-9]+")) {
                    String imagensNDescom = imagens[i].replaceAll("_DESCOM", "");
                    System.out.format("%d%s%s%n", i, " - ", imagensNDescom.trim());
                } else {
                    System.out.format("%d%s%s%n", i, " - ", imagens[i].trim());
                }
            }
            do {
                System.out.format("%s%n%s%d%n", "Insira a opção", "*por favor escolha um número de 1 a ", imagens.length - 1);
                String resposta = scan.nextLine();
                if (resposta.matches("[1-9][0-9]*")) {
                    opcao = Integer.parseInt(resposta);
                }
            } while (opcao > imagens.length - 1 || opcao < 0);
            Runtime rt = Runtime.getRuntime();
            rt.exec("gnuplot -e \"filename=\'" + imagens[opcao].trim() + "\'\"  script.gp");
            Thread.sleep(5000);
            menuVisualizador(matriz, nome, nomeFich,directory,isFile);
        }
    }

    /**
     * Procura as imagens no directory em questão e guarda apenas as que são
     * validas. Se houve imagens compressadas, decompressa e guarda num novo
     * ficheiro pronto para ser lido pelo gnuplot Retorna uma String com os
     * nomes de todos os ficheiros válidos
     *
     * @param directory - nome do diretório onde vão ser procuradas as imagens
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String procurarImagens(String directory) throws FileNotFoundException, IOException {
        String textFiles = "Lista de Imagens:";
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith(".txt")) {
                String testeCompre = verificarMatrizComp(file.getName(), directory);
                String testeMatr = verificarMatriz(file.getName(), directory);
                if (testeMatr.matches("PASS")) {
                    textFiles = textFiles + "&" + file.getName();
                }
                if (testeCompre.matches("[0-9]+")) {
                    menuDescompressao(file.getName(), directory);
                }
            }
        }
        return textFiles;
    }
}
