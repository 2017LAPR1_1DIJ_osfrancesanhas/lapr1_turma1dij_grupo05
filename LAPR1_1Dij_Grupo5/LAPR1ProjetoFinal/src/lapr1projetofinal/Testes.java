package lapr1projetofinal;

import java.io.FileNotFoundException;
import java.io.IOException;
import lapr1projetofinal.LAPR1ProjetoFinal;

public class Testes {

    public final static String nomeFich = "Imagem.txt";
    public final static String[] nomeFich1 = {"Imagem.txt"};
    public final static String nomeFichErrado = "ImagemErrada0.txt";
    public final static String nomeFichComp = "Imagem_3Comp.txt";
    public final static String nome = "teste";
    public final static String directory = System.getProperty("user.dir");
    //se apagar alguns ficheiros altere de acordo com as mudanças também a Sring stringFicheirosPasta
    public final static String stringFicheirosPasta = "Lista de Imagens:&Imagem.txt&Imagem_3COMP_DESCOM.txt&Imagem_Maximo_Media.txt&Imagem_Maximo_Media_RotacaoSentidoHorario.txt&Imagem_Maximo_Media_RotacaoSentidoHorario_Media.txt&Imagem_Media.txt&Imagem_Mediana.txt&Imagem_Media_Mediana.txt&Imagem_Ordenação.txt&Imagem_RotacaoSentidoHorario.txt&Imagem_RotacaoSentidoHorario_Mediana.txt&Imagem_RotacaoSentidoHorario_Mediana_Variancia_RotacaoSentidoHorario.txt&Imagem_RotacaoSentidoHorario_Mediana_Variancia_RotacaoSentidoHorario_Ordenação.txt&Imagem_RotacaoSentidoHorario_Mediana_Variancia_RotacaoSentidoHorario_Ordenação_RotacaoSentidoHorario.txt&ImageTeste_Maximo.txt";
    public final static int sizeFich = 4;
    public final static int size = 3;
    public final static int valores = 3;
    public final static int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    public final static double[][] matU = {{0.64,0.77,-0.01},{0.47, -0.38, -0.04,},{0.45, -0.36, 0.72},{0.42,-0.36,-0.70}};
    public final static double[][] matV = {{0.41,-0.33,0.77},{0.45,-0.36,-0.01},{0.47,-0.42,-0.63},{0.64,0.77,-0.02}};
    public final static double[][] matD = {{535.79,0,0},{0,62.77,0},{0,0,4.89}};
    public final static int[][] matrixDescom = {{125, 137, 141, 255}, {111, 122, 128, 143}, {109, 117, 121, 137}, {97, 109, 117, 127}};
    public final static int[] vetor = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    public final static int somaElementosMatriz = 45;
    public final static int mediaElementosVetor = 5;
    public final static int medianaElementosVetor = 5;
    public final static int minimoElementosVetor = 1;
    public final static int maximoElementosVetor = 9;
    public final static int convolucaoElementosVetor = 5;
    public final static int varianciaElementosVetor = 7;
    public final static int[] vetorValores0e0 = {0, 1, 2, 1, 1, 2, 4, 4, 5};
    public final static int[] vetorValores0e1 = {1, 2, 3, 1, 2, 3, 4, 5, 6};
    public final static int[] vetorValores1e0 = {1, 1, 2, 4, 4, 5, 7, 7, 8};
    public final static int[] vetorValores1e1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    public final static float[] VetorKernel = {0, -1, 0, -1, 5, -1, 0, -1, 0};
    public final static int[][] matrixRotacaoHoraria = {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}};
    public final static int[][] matrixRotacaoAntiH = {{3, 6, 9}, {2, 5, 8}, {1, 4, 7}};
    public final static int[][] matrizDes = {{4, 2, 3}, {6, 8, 1}, {7, 5, 9}};
    public final static int[][] matrizOrd = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};
    public final static int[] vetorDes = {4, 2, 6, 7, 5, 9, 3, 8, 1};
    public final static int[] vetorOrd = {9, 8, 7, 6, 5, 4, 3, 2, 1};

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String result;
        boolean resultBoolean;
        int resultInt;
        float reultFloat;
        int[] resultArray;
        int[][] resultMatrix;
        int j, i, test, cont = 0, pass = 0;

        resultBoolean = LAPR1ProjetoFinal.checkIfFileExist(nomeFich);
        cont++;
        if (resultBoolean == true) {
            System.out.format("%s%n", "checkIfFileExist passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "checkIfFileExist não passou o teste!");
        }

        resultBoolean = LAPR1ProjetoFinal.checkIfDirExist(directory);
        cont++;
        if (resultBoolean == true) {
            System.out.format("%s%n", "checkIfDirExist passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "checkIfDirExist não passou o teste!");
        }

        result = LAPR1ProjetoFinal.verificarMatrizComp(nomeFichComp, directory);
        cont++;
        if (result.matches("[0-9]+")) {
            System.out.format("%s%n", "verificarMatrizComp passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "verificarMatrizComp não passou o teste!");
        }

        result = LAPR1ProjetoFinal.verificarMatrizComp(nomeFich, directory);
        cont++;
        if (!result.matches("[0-9]+")) {
            System.out.format("%s%n", "verificarMatrizComp passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "verificarMatrizComp não passou o teste!");
        }

        result = LAPR1ProjetoFinal.verificarMatriz(nomeFich, directory);
        cont++;
        if (result.equals("PASS")) {
            System.out.format("%s%n", "verificarMatriz passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "verificarMatriz não passou o teste!");
        }

        result = LAPR1ProjetoFinal.verificarMatriz(nomeFichErrado, directory);
        cont++;
        if (!result.equals("PASS")) {
            System.out.format("%s%n", "verificarMatriz passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "verificarMatriz não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.estatisticasMatriz(matrix, size);
        cont++;
        if (resultInt == somaElementosMatriz) {
            System.out.format("%s%n", "estatisticasMatriz passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "estatisticasMatriz não passou o teste!");
        }

        i = 0;
        j = 0;
        resultArray = LAPR1ProjetoFinal.guardarVetorValores(matrix, size, i, j);
        cont++;
        test = 1;
        for (int y = 0; y < resultArray.length; y++) {
            if (resultArray[y] != vetorValores0e0[y]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "guardarVetorValores passou o teste nº1!");
            pass++;
        } else {
            System.out.format("%s%n", "guardarVetorValores não passou o teste nº1!");
        }

        i = 1;
        j = 0;
        resultArray = LAPR1ProjetoFinal.guardarVetorValores(matrix, size, i, j);
        cont++;
        test = 1;
        for (int y = 0; y < resultArray.length; y++) {
            if (resultArray[y] != vetorValores1e0[y]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "guardarVetorValores passou o teste nº2!");
            pass++;
        } else {
            System.out.format("%s%n", "guardarVetorValores não passou o teste nº2!");
        }

        i = 0;
        j = 1;
        resultArray = LAPR1ProjetoFinal.guardarVetorValores(matrix, size, i, j);
        cont++;
        test = 1;
        for (int y = 0; y < resultArray.length; y++) {
            if (resultArray[y] != vetorValores0e1[y]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "guardarVetorValores passou o teste nº3!");
            pass++;
        } else {
            System.out.format("%s%n", "guardarVetorValores não passou o teste nº3!");
        }

        i = 1;
        j = 1;
        resultArray = LAPR1ProjetoFinal.guardarVetorValores(matrix, size, i, j);
        cont++;
        test = 1;
        for (int y = 0; y < resultArray.length; y++) {
            if (resultArray[y] != vetorValores1e1[y]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "guardarVetorValores passou o teste nº4!");
            pass++;
        } else {
            System.out.format("%s%n", "guardarVetorValores não passou o teste nº4!");
        }

        resultInt = LAPR1ProjetoFinal.filtroMedia(vetor);
        cont++;
        if (resultInt == mediaElementosVetor) {
            System.out.format("%s%n", "filtroMedia passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroMedia não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.filtroMediana(vetor);
        cont++;
        if (resultInt == medianaElementosVetor) {
            System.out.format("%s%n", "filtroMediana passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroMediana não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.filtroMinimo(vetor);
        cont++;
        if (resultInt == minimoElementosVetor) {
            System.out.format("%s%n", "filtroMinimo passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroMinimo não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.filtroMaximo(vetor);
        cont++;
        if (resultInt == maximoElementosVetor) {
            System.out.format("%s%n", "filtroMaximo passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroMaximo não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.filtroConvolucao(vetor, VetorKernel);
        cont++;
        if (resultInt == convolucaoElementosVetor) {
            System.out.format("%s%n", "filtroConvolucao passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroConvolucao não passou o teste!");
        }

        resultInt = LAPR1ProjetoFinal.filtroVariancia(vetor);
        cont++;
        if (resultInt == varianciaElementosVetor) {
            System.out.format("%s%n", "filtroVariancia passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroVariancia não passou o teste!");
        }

        test = 1;
        resultMatrix = LAPR1ProjetoFinal.filtroRotacaoHoraria(matrix, size, nomeFich, nome);
        cont++;
        for (int x = 0; x < resultMatrix.length; x++) {
            for (int y = 0; y < resultMatrix[x].length; y++) {
                if (resultMatrix[x][y] != matrixRotacaoHoraria[x][y]) {
                    test = 0;
                }
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "filtroRotacaoHoraria passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroRotacaoHoraria não passou o teste!");
        }

        test = 1;
        resultMatrix = LAPR1ProjetoFinal.filtroRotacaoAntiH(matrix, size, nomeFich, nome);
        cont++;
        for (int x = 0; x < resultMatrix.length; x++) {
            for (int y = 0; y < resultMatrix[x].length; y++) {
                if (resultMatrix[x][y] != matrixRotacaoAntiH[x][y]) {
                    test = 0;
                }
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "filtroRotacaoAntiH passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtroRotacaoAntiH não passou o teste!");
        }

        resultMatrix = LAPR1ProjetoFinal.filtrosSort(matrizDes, size, nomeFich, nome);
        cont++;
        for (int x = 0; x < resultMatrix.length; x++) {
            for (int y = 0; y < resultMatrix[x].length; y++) {
                if (resultMatrix[x][y] != matrizOrd[x][y]) {
                    test = 0;
                }
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "filtrosSort passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "filtrosSort não passou o teste!");
        }

        test = 1;
        resultArray = LAPR1ProjetoFinal.doSelectionSort(vetorDes);
        cont++;
        for (int x = 0; x < resultArray.length; x++) {
            if (resultArray[x] != vetorOrd[x]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "doSelectionSort passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "doSelectionSort não passou o teste!");
        }

        resultMatrix = LAPR1ProjetoFinal.descompressao(matU,matD,matV,valores,sizeFich);
        cont++;
        for (int x = 0; x < resultMatrix.length; x++) {
            for (int y = 0; y < resultMatrix[x].length; y++) {
                if (resultMatrix[x][y] != matrixDescom[x][y]) {
                test = 0;
                }
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "descompressao passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "descompressao não passou o teste!");
        }

        resultArray = LAPR1ProjetoFinal.matrizParaVetor(matrix, size);
        cont++;
        for (int x = 0; x < resultArray.length; x++) {
            if (resultArray[x] != vetor[x]) {
                test = 0;
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "matrizParaVetor passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "matrizParaVetor não passou o teste!");
        }

        resultMatrix = LAPR1ProjetoFinal.vetorParaMatriz(vetor, size, matrix, nomeFich, nome);
        cont++;
        for (int x = 0; x < resultMatrix.length; x++) {
            for (int y = 0; y < resultMatrix[x].length; y++) {
                if (resultMatrix[x][y] != matrix[x][y]) {
                    test = 0;
                }
            }
        }
        if (test == 1) {
            System.out.format("%s%n", "vetorParaMatriz passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "vetorParaMatriz não passou o teste!");
        }

        float percPass = (float) pass / cont * 100;
        System.out.format("%s%.2f%s", "O programa LAPR1ProjetoFinal passou a ", percPass, "% dos testes.");
        
        result = LAPR1ProjetoFinal.procurarImagens(directory);
        cont++;
        if (result.equals(stringFicheirosPasta)) {
            System.out.format("%s%n", "vetorParaMatriz passou o teste!");
            pass++;
        } else {
            System.out.format("%s%n", "vetorParaMatriz não passou o teste!");
        }
    }
}
