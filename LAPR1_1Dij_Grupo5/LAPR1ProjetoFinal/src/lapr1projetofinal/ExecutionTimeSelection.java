package lapr1projetofinal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import static lapr1projetofinal.Constantes.*;

public class ExecutionTimeSelection {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException {
        long lStartTime = System.nanoTime();
        calculation();
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in nanoseconds: " + output);
    }
    private static void calculation() throws InterruptedException, FileNotFoundException {
        lerMatriz();
    }
        
    public static void lerMatriz() throws FileNotFoundException {
        Scanner fInput = new Scanner(new File("testImage200.txt"));
        String linha0 = fInput.nextLine();
        String[] linhaDiv = linha0.split(ESPAÇAMENTO_TÍTULO);
        int size = Integer.parseInt(linhaDiv[1]);
        int[][] matriz = new int[size][size];
        fInput.nextLine();
        for (int i = 0; i < size; i++) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            String[] temp = linha.split(SEPARADOR_MATRIZ);
            for (int j = 0; j < size; j++) {
                matriz[i][j] = Integer.parseInt(temp[j]);
            }
        }
        int[] vetorTotal = matrizParaVetor(matriz, size);
        int[] vetorOrde = doSelectionSort(vetorTotal);
        vetorParaMatriz(vetorOrde, size);
    }

    public static int[] doSelectionSort(int[] matriz){
         
        for (int i = 0; i < matriz.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < matriz.length; j++)
                if (matriz[j] < matriz[index]) 
                    index = j;
      
            int smallerNumber = matriz[index];  
            matriz[index] = matriz[i];
            matriz[i] = smallerNumber;
        }
        return matriz;
    }

    public static int[] matrizParaVetor(int matriz[][], int size) {
        int[] vetor = new int[size * size];
        int cont = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                vetor[cont] = matriz[i][j];
                cont = cont + 1;
            }
        }
        return vetor;
    }

    public static void vetorParaMatriz(int[] vetor, int size) {
        int[][] matrizTransformada = new int[size][size];
        int contador = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizTransformada[i][j] = vetor[contador];
                contador++;
            }
        }
        listarMatriz(matrizTransformada);
    }

    public static void listarMatriz(int[][] matriz) {
        System.out.println("Listagem da matriz");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}