package lapr1projetofinal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;
import java.util.Scanner;
import static lapr1projetofinal.Constantes.*;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;

public class LAPR1ProjetoFinal {

    public static Scanner scan = new Scanner(System.in);

    /**
     * O programa começa por invocar o metodo lerFicheiro Com isto guarda-se, se
     * possível, os parametros da primeira linha e testa-se o ficheiro em
     * questão até este ser válido Mostra-se o menu principal ao utilizador
     *
     * @param args parametro dado pelo utilizador
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String linha = lerFicheiro(args);
        String[] titulo = linha.split(ESPAÇAMENTO_TÍTULO);
        String nome = titulo[0];
        int size = Integer.parseInt(titulo[1]);
        int[][] matriz = new int[size][size];
        String nomeFich = titulo[2];
        lerMatriz(nomeFich, size, matriz);
        nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf("."));
        menuPrincipal(matriz, size, nomeFich, nome);
    }

    /**
     * Deixa o utilizador ver os resultados antes de escolher nova opção
     *
     */
    private static void pausa() {
        System.out.println("\nPara continuar digite ENTER");
        scan.nextLine();
    }

    /**
     * Pede o nome do ficheiro ao utilizador. Enquanto que o ficheiro dado tiver
     * erros, estiver mal formatado ou não exita o programa vai pedir novo nome
     * de ficheiro.
     *
     * @param args parametro dado pelo utilizador
     * @return String com a primeira linha do ficheiro e o nome do ficheiro
     * @throws FileNotFoundException
     */
    public static String lerFicheiro(String[] args) throws FileNotFoundException {
        boolean matrizPossivel;
        String linha;
        String nomeFich;
        do {
            nomeFich = args[0];
            nomeFich = checkIfFileExist(nomeFich);
            Scanner fInput = new Scanner(new File(nomeFich));
            linha = fInput.nextLine().replaceAll("[ ][ ]+", ESPAÇAMENTO_TÍTULO).trim();
            String[] titulo = linha.split(ESPAÇAMENTO_TÍTULO);
            titulo[1] = titulo[1].replaceAll("[^0-9]+", "1");
            int size = Integer.parseInt(titulo[1]);
            matrizPossivel = verificarMatriz(nomeFich, size);
        } while (matrizPossivel == false);
        linha = linha + ESPAÇAMENTO_TÍTULO + nomeFich;
        return linha;
    }

    /**
     * Testa se o nome dado pelo utilizador corresponde a um ficheiro txt
     * existente. Enquanto que este não for encontrado vai pedir novo nome de
     * ficheiro
     *
     * @param nomeFich - String com o nome do ficheiro
     * @return nome de um ficheiro existente
     */
    public static String checkIfFileExist(String nomeFich) {
        File f = new File(nomeFich);
        while (!f.exists() || f.isDirectory()) {
            System.out.format("%s%n%s%n", "Ficheiro não encontrado!", "Insira nome do ficheiro novamente");
            nomeFich = scan.nextLine();
            f = new File(nomeFich);
        }
        return nomeFich;
    }

    /**
     * Verifica se o ficheiro está bem formatado e retorna um boolean dependendo
     * disto Verifica se: -A primeira linha só tem o nome da imagem e as
     * dimensões da matriz separados por dois espaços -A segunda linha está
     * vazia -A matriz tem as dimensões dadas no título -A matriz é só composta
     * por números de 0 a 255
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param size - dimensões da matriz
     * @return resposta true ou false
     * @throws FileNotFoundException
     */
    public static boolean verificarMatriz(String nomeFich, int size) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        String line0 = fInput.nextLine().trim();
        if (!line0.matches(REGEX_TÍTULO_TXT)) {
            System.out.format("%s%n", "Título inválido1");
            return false;
        }
        String[] titulo = line0.split(ESPAÇAMENTO_TÍTULO);
        if (titulo.length != 2) {
            System.out.format("%s%n", "Título inválido2");
            return false;
        }
        String line1 = fInput.nextLine();
        if (!line1.trim().isEmpty()) {
            System.out.format("%s%n", "Documento inválido2");
            return false;
        }
        int linhas = 0;
        while (fInput.hasNextLine()) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            linhas++;
            String[] linha1 = linha.split(SEPARADOR_MATRIZ);
            if (linha1.length != size) {
                System.out.format("%s%n", "Matriz inválida");
                return false;
            } else {
                for (int i = 0; i < size; i++) {
                    if (linha1[i].matches("[0-9]+")) {
                        int num = Integer.parseInt(linha1[i]);
                        if (num < Constantes.LIM_MÍNIMO || num > Constantes.LIM_MÁXIMO) {
                            System.out.format("%s%n", "Matriz inválida");
                            return false;
                        }
                    } else {
                        System.out.format("%s%n", "Matriz inválida");
                        return false;
                    }
                }
            }
        }
        if (linhas != size) {
            System.out.format("%s%n", "Matriz inválida");
            return false;
        }
        return true;
    }

    /**
     * Guarda a matriz dada no ficheiro em memória, no parametro "matriz"
     *
     * @param nomeFich - nome do ficheiro em questão
     * @param size - dimensões da matriz
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @throws FileNotFoundException
     */
    public static void lerMatriz(String nomeFich, int size, int[][] matriz) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        fInput.nextLine();
        fInput.nextLine();
        for (int i = 0; i < size; i++) {
            String linha = fInput.nextLine();
            linha = linha.replaceAll(" +", "");
            String[] temp = linha.split(",");
            for (int j = 0; j < size; j++) {
                matriz[i][j] = Integer.parseInt(temp[j]);
            }
        }
    }

    /**
     * Menu principal, dergunta ao utilizador se quer ver as estatísticas da
     * matriz ou aplicar filtros O programa pára se escolhermos a opção 0 - Sair
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     */
    public static void menuPrincipal(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        String op;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Estatísticas",
                    "2 - Filtros",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    menuEstatistica(matriz, size);
                    break;
                case "2":
                    menuFiltros(matriz, size, nomeFich, nome);
                    break;
                case "0":
                    System.out.format("%n%s%n%s%n", "Muito Obrigado!", "Fim");
                    System.exit(0);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Menu dos calculos estatisticos Voltamos ao menu principal ao escolher a
     * opção 0
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void menuEstatistica(int[][] matriz, int size) {
        String op;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Soma e média de cada uma das linhas",
                    "2 - Soma e média de cada uma das colunas",
                    "3 - Soma e média de todos os elementos da matriz",
                    "4 - Conjunto de valores e vetores próprios",
                    "5 - Regra dos trapézios simples",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    estatisticasLinhas(matriz, size);
                    pausa();
                    break;
                case "2":
                    estatisticasColunas(matriz, size);
                    pausa();
                    break;
                case "3":
                    int soma = estatisticasMatriz(matriz, size);
                    float media = (float) soma / (size * size);
                    System.out.format("%s%d%n","Soma de todos os elementos da matriz: ",soma);
                    System.out.format("%s%.2f%n", "Média de todos os elementos da matriz: ",media);
                    pausa();
                    break;
                case "4":
                    double[][] matrizDouble = new double[size][size];
                    vetorValorProprio(matrizDouble, matriz, size);
                    pausa();
                    break;
                case "5":
                    estatisticasTrapezio(matriz, size);
                    pausa();
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }
    
    /**
     * Calcula a média e soma de cada coluna Mostra dois valores devidamente
     * carcterizados para cada coluna da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void estatisticasColunas(int[][] matriz, int size) {
        int somaColuna = 0;
        float mediaColuna;
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                somaColuna += matriz[j][i];
            }
            mediaColuna = (float) somaColuna / size;
            System.out.format("%s%d%s%d%n", "Soma da coluna ", i + 1, ": ", somaColuna);
            System.out.format("%s%d%s%.2f%n", "Média da coluna ", i + 1, ": ", mediaColuna);
            somaColuna = 0;
        }
    }

    /**
     * Calcula a média e soma de cada linha Mostra dois valores devidamente
     * carcterizados para cada linha da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     */
    public static void estatisticasLinhas(int[][] matriz, int size) {
        int somaLinha = 0;
        float mediaLinha;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                somaLinha += matriz[i][j];
            }
            mediaLinha = (float) somaLinha / size;
            System.out.format("%s%d%s%d%n", "Soma da linha ", i + 1, ": ", somaLinha);
            System.out.format("%s%d%s%.2f%n", "Média da linha ", i + 1, ": ", mediaLinha);
            somaLinha = 0;
        }
    }

    /**
     * Calcula e retorna a soma dos elementos da matriz
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @return soma dos elementos da matriz
     */
    public static int estatisticasMatriz(int[][] matriz, int size) {
        int soma = 0;
        float media = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                soma += matriz[i][j];
            }
        }
        return soma;
    }

    /**
     * Criar objeto do tipo Matrix com a linguagem da livraria Obtem valores e
     * vetores próprios fazendo "Eigen Decomposition" converte objeto Matrix
     * (duas matrizes) para array Java
     *
     * @param matriz - matriz dada no ficheiro, guardada em memória
     * @param size - dimensões da matriz
     * @param matrizDouble - matriz criada para converter a matriz de inteiros
     * em double
     */
    public static void vetorValorProprio(double[][] matrizDouble, int[][] matriz, int size) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizDouble[i][j] = (double) matriz[i][j];
            }
        }
        Matrix a = new Basic2DMatrix(matrizDouble);
        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] Decompose = eigenD.decompose();
        double matB[][] = Decompose[1].toDenseMatrix().toArray();
        System.out.format("%s%n", "Valores próprios:");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matB[i][j] != 0) {
                    System.out.format("%.2f%n", matB[i][j]);
                }
            }
        }
        System.out.format("%n");
        double matA[][] = Decompose[0].toDenseMatrix().toArray();
        System.out.format("%s%n", "Vetores próprios:");
        for (int i = 0; i < matriz.length; i++) {
            System.out.format("%s", "{(");
            for (int j = 0; j < matriz.length - 1; j++) {
                System.out.format("%.2f%s", matA[j][i], ",");
            }
            System.out.format("%.2f", matA[matriz.length - 1][i]);
            System.out.format("%s%n", ")}");
        }
        System.out.format("%n");
    }
    
    public static void estatisticasTrapezio(int [][] matriz, int size) {
        int soma = 0;
        float trapezioLinha;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                soma = soma + matriz[i][j];
            }
            for (int l = 1; l < size - 1; l++) {
                soma = soma + matriz[i][l];
            }
            trapezioLinha = (float) soma / 2;
            System.out.format("%s%d%s%.1f%n", "Soma da linha ", i + 1, ": ", trapezioLinha);
        }
    }

    /**
     * Menu da escolha de filtros Voltamos ao menu principal ao escolher a opção
     * 0
     *
     * @param mat - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     */
    public static void menuFiltros(int[][] mat, int size, String nomeFich, String nome) throws IOException {
        String op;
        String message;
        int[][] matAlterada = new int[size][size];
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Média",
                    "2 - Mediana",
                    "3 - Máximo",
                    "4 - Mínimo",
                    "5 - Rotação",
                    "6 - Ordenação",
                    "7 - Convolução",
                    "8 - Variância",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Media");
                    break;
                case "2":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Mediana");
                    break;
                case "3":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Maximo");
                    break;
                case "4":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Minimo");
                    break;
                case "5":
                    menuRotacao(mat, size, nomeFich, nome);
                    break;
                case "6":
                    matAlterada = filtrosSort(mat, size, nomeFich, nome);
                    listarMatriz(matAlterada);
                    message = "Ordenação";
                    nomeFich = (nomeFich + "_" + message);
                    menuFiltrosOp(mat, matAlterada, size, nomeFich, nome);
                    break;
                case "7":
                    aplicarFiltros(mat, matAlterada, size, nome, nomeFich, op, "Convolução");
                    break;
                case "8":
                    
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuPrincipal(mat, size, nomeFich, nome);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Aplica o filtro escolhido no menu à matriz guardada, pixel a pixel, como
     * pedido (3x3) Mostra a matriz alterada ao utilizador Grava as alterações
     * em memória na nova matriz matAlterada
     *
     * @param mat - matriz guardada em memória
     * @param matAlterada - matriz alterada pelos filtros
     * @param size - dimensões da matriz
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @param nomeFich - nome do ficheiro em questão
     * @param op - opção escolhida pelo utilizador no menu dos filtros
     * @param message - tipo de filtro utilizado ( relacionado com a opção
     * escolhida)
     * @throws IOException
     */
    public static void aplicarFiltros(int[][] mat, int[][] matAlterada, int size, String nome, String nomeFich, String op, String message) throws IOException {
        int [] vetorKernel = new int [9];
        if (op.equals("7")) {
            vetorKernel = pedirMatrizKernel(vetorKernel);
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int vetorValores[] = guardarVetorValores(mat, size, i, j);
                switch (op) {
                    case "1":
                        matAlterada[i][j] = filtroMedia(vetorValores);
                        break;
                    case "2":
                        matAlterada[i][j] = filtroMediana(vetorValores);
                        break;
                    case "3":
                        matAlterada[i][j] = filtroMaximo(vetorValores);
                        break;
                    case "4":
                        matAlterada[i][j] = filtroMinimo(vetorValores);
                        break;
                    case "7":
                        matAlterada[i][j] = filtroConvolucao(vetorValores,vetorKernel);
                        break;
                }
            }
        }
        listarMatriz(matAlterada);
        nomeFich = (nomeFich + "_" + message);
        menuFiltrosOp(mat, matAlterada, size, nomeFich, nome);
    }

    /**
     * Apresenta um novo menu ao utilizador com as opções: -Criar novo ficheiro
     * com a matriz alterada pelo filtros escolhido -Apaga a matriz alterada e
     * volta a pedir ao utilizador um filtro para aplicar na matriz inalterada
     * pelo ultimo filtro -Aplica um novo filtro à matriz já alterada -Apaga a
     * matriz alterada e volta ao menu principal
     *
     * @param matriz - matriz original guardada em memória
     * @param matrizAlterada - matriz alterada pelos filtros
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     */
    public static void menuFiltrosOp(int[][] matriz, int[][] matrizAlterada, int size, String nomeFich, String nome) throws IOException {
        String op;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n%s%n", "Pretende:",
                    "1- Guardar o filtro",
                    "2- Escolher um filtro diferente",
                    "3- Escolher mais filtros para aplicar",
                    "0- Sair sem gravar alterações");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    guardarNovoFicheiro(matrizAlterada, size, nomeFich, nome);
                    System.out.format("%n%s%n%s%n", "Alterações gravadas", "Obrigado!");
                    break;
                case "2":
                    nomeFich = nomeFich.substring(0, nomeFich.lastIndexOf("_"));
                    menuFiltros(matriz, size, nomeFich, nome);
                    break;
                case "3":
                    menuFiltros(matrizAlterada, size, nomeFich, nome);
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuPrincipal(matriz, size, nomeFich, nome);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Retira da matriz dada 9 valores que correspondem ao pixel na posição
     * (i,j) e os 8 valores que o rodeiam No caso de o pixel estar na
     * "fronteira" da matriz os valores que não existem são criados com base nos
     * valores disponíveis Retorna esses 9 valores guardados num vector
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param i - linha do pixel a alterar
     * @param j - coluna do pixel a alterar
     * @return vetor com os 9 valores em questão
     */
    public static int[] guardarVetorValores(int[][] matriz, int size, int i, int j) {
        int[] vetorValores = new int[9];
        int cont = 0;
        if (i == 0) {
            if (j == 0) {
                for (cont = 0; cont < 3; cont++) {
                    vetorValores[cont] = matriz[i][j];
                }
                for (cont = 3; cont < 7; cont = cont + 2) {
                    vetorValores[cont] = matriz[i][j + 1];
                    vetorValores[cont + 1] = matriz[i + 1][j];
                }
                vetorValores[7] = matriz[i + 1][j + 1];
                vetorValores[8] = 0;
            } else if (j == size - 1) {
                for (cont = 0; cont < 3; cont++) {
                    vetorValores[cont] = matriz[i][j];
                }
                for (cont = 3; cont < 7; cont = cont + 2) {
                    vetorValores[cont] = matriz[i][j - 1];
                    vetorValores[cont + 1] = matriz[i + 1][j];
                }
                vetorValores[7] = matriz[i + 1][j - 1];
                vetorValores[8] = 0;

            } else {
                for (cont = 0; cont < 6; cont = cont + 3) {
                    vetorValores[cont] = matriz[i][j];
                    vetorValores[cont + 1] = matriz[i][j - 1];
                    vetorValores[cont + 2] = matriz[i][j + 1];
                }
                vetorValores[6] = matriz[i + 1][j - 1];
                vetorValores[7] = matriz[i + 1][j + 1];
                vetorValores[8] = matriz[i + 1][j];
            }
        } else if (i == size - 1) {
            if (j == 0) {
                for (cont = 0; cont < 3; cont++) {
                    vetorValores[cont] = matriz[i][j];
                }
                for (cont = 3; cont < 7; cont = cont + 2) {
                    vetorValores[cont] = matriz[i][j + 1];
                    vetorValores[cont + 1] = matriz[i - 1][j];
                }
                vetorValores[7] = matriz[i - 1][j + 1];
                vetorValores[8] = 0;
            } else if (j == size - 1) {
                for (cont = 0; cont < 3; cont++) {
                    vetorValores[cont] = matriz[i][j];
                }
                for (cont = 3; cont < 7; cont = cont + 2) {
                    vetorValores[cont] = matriz[i][j - 1];
                    vetorValores[cont + 1] = matriz[i - 1][j];
                }
                vetorValores[7] = matriz[i - 1][j - 1];
                vetorValores[8] = 0;
            } else {
                for (cont = 0; cont < 6; cont = cont + 3) {
                    vetorValores[cont] = matriz[i][j];
                    vetorValores[cont + 1] = matriz[i][j - 1];
                    vetorValores[cont + 2] = matriz[i][j + 1];
                }
                vetorValores[6] = matriz[i - 1][j - 1];
                vetorValores[7] = matriz[i - 1][j + 1];
                vetorValores[8] = matriz[i - 1][j];
            }
        } else {
            if (j == 0) {
                for (cont = 0; cont < 6; cont = cont + 3) {
                    vetorValores[cont] = matriz[i][j];
                    vetorValores[cont + 1] = matriz[i - 1][j];
                    vetorValores[cont + 2] = matriz[i + 1][j];
                }
                vetorValores[6] = matriz[i - 1][j + 1];
                vetorValores[7] = matriz[i + 1][j + 1];
                vetorValores[8] = matriz[i][j + 1];
            } else if (j == size - 1) {
                for (cont = 0; cont < 6; cont = cont + 3) {
                    vetorValores[cont] = matriz[i][j];
                    vetorValores[cont + 1] = matriz[i - 1][j];
                    vetorValores[cont + 2] = matriz[i + 1][j];
                }
                vetorValores[6] = matriz[i - 1][j - 1];
                vetorValores[7] = matriz[i + 1][j - 1];
                vetorValores[8] = matriz[i][j - 1];
            } else {
                for (int i2 = -1; i2 <= 1; i2++) {
                    for (int j2 = -1; j2 <= 1; j2++) {
                        vetorValores[cont] = matriz[i + i2][j + j2];
                        cont = cont + 1;
                    }
                }
            }
        }
        return vetorValores;
    }

    /**
     * Faz a média dos 9 valores dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMedia(int[] vetorValores) {
        int soma = 0;
        for (int i = 0; i < vetorValores.length; i++) {
            soma = soma + vetorValores[i];
        }
        int media = soma / vetorValores.length;
        return media;
    }

    /**
     * Faz a mediana dos 9 valores dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMediana(int[] vetorValores) {
        for (int i = 0; i < vetorValores.length; i++) {
            for (int j = i + 1; j < vetorValores.length; j++) {
                int tmp;
                if (vetorValores[i] > vetorValores[j]) {
                    tmp = vetorValores[i];
                    vetorValores[i] = vetorValores[j];
                    vetorValores[j] = tmp;
                }
            }
        }
        return vetorValores[4];
    }

    /**
     * Procura o valor mais alto dos 9 dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMaximo(int[] vetorValores) {
        int valorMaximo = -1;
        for (int i = 0; i < vetorValores.length; i++) {
            if (valorMaximo < vetorValores[i]) {
                valorMaximo = vetorValores[i];
            }
        }
        return valorMaximo;
    }

    /**
     * Procura o valor mais baixo dos 9 dados e retorna esse valor
     *
     * @param vetorValores - vetor com os 9 valores retirados no metodo
     * guardarVetorValores
     * @return valor que vai substituir na nova matriz o pixel que está a ser
     * alterado
     */
    public static int filtroMinimo(int[] vetorValores) {
        int valorMinimo = 256;
        for (int i = 0; i < vetorValores.length; i++) {
            if (valorMinimo > vetorValores[i]) {
                valorMinimo = vetorValores[i];
            }
        }
        return valorMinimo;
    }

    /**
     * Apresenta um menu que deixa o utilizador escolher se quer rodar a matriz
     * 90º para a direita ou esquerda
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     */
    public static void menuRotacao(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        String op;
        String message;
        do {
            System.out.format("%n%s%n%s%n%s%n%s%n", "Escolha uma opção:",
                    "1 - Rotação no sentido horário",
                    "2 - Rotação no sentido anti-horário",
                    "0 - Sair");
            op = scan.nextLine();
            switch (op) {
                case "1":
                    int[][] matrizRodadaH = filtroRotacaoHoraria(matriz, size, nomeFich, nome);
                    listarMatriz(matrizRodadaH);
                    message = "RotacaoSentidoHorario";
                    nomeFich = (nomeFich + "_" + message);
                    menuFiltrosOp(matriz, matrizRodadaH, size, nomeFich, nome);
                    break;
                case "2":
                    int[][] matrizRodadaA = filtroRotacaoAntiH(matriz, size, nomeFich, nome);
                    listarMatriz(matrizRodadaA);
                    message = "RotacaoSentidoAnti-Horario";
                    nomeFich = (nomeFich + "_" + message);
                    menuFiltrosOp(matriz, matrizRodadaA, size, nomeFich, nome);
                    break;
                case "0":
                    System.out.println("Obrigado!");
                    menuFiltrosOp(matriz, matriz, size, nomeFich, nome);
                    break;
                default:
                    System.out.format("%n%s%n%s%n", "Opção incorreta.", "Por favor repita");
                    break;
            }
        } while (!op.equals("0"));
    }

    /**
     * Rota a matriz dada para a direita
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz original rodada para a direita
     * @throws IOException
     */
    public static int[][] filtroRotacaoHoraria(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[][] matrizRodadaH = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizRodadaH[i][j] = matriz[size - j - 1][i];
            }
        }
        return matrizRodadaH;
    }

    /**
     * Rota a matriz dada para a esquerda
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz original rodada para a esquerda
     * @throws IOException
     */
    public static int[][] filtroRotacaoAntiH(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[][] matrizRodadaA = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizRodadaA[i][j] = matriz[j][size - i - 1];
            }
        }
        return matrizRodadaA;
    }

    /**
     * Ordena os valores da matriz do maior ao mais pequeno Passando primeiro os
     * valores para um vector
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz com o filtro já aplicado
     * @throws IOException
     */
    public static int[][] filtrosSort(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        int[] vetorTotal = matrizParaVetor(matriz, size);
        int[] vetorOrde = doSelectionSort(vetorTotal);
        int[][] matrizTransformada = vetorParaMatriz(vetorOrde, size, matriz, nomeFich, nome);
        return matrizTransformada;
    }

    /**
     * Ordena os valores do vector matriz do maior para o menor
     *
     * @param matriz - vetor com todos os valores da matriz
     * @return vector de inteiros com todos os valores da matriz
     */
    public static int[] doSelectionSort(int[] matriz) {

        for (int i = 0; i < matriz.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < matriz.length; j++) {
                if (matriz[j] > matriz[index]) {
                    index = j;
                }
            }

            int smallerNumber = matriz[index];
            matriz[index] = matriz[i];
            matriz[i] = smallerNumber;
        }
        return matriz;
    }

    /**
     * Guarda os valores da matriz num vector de inteiros
     *
     * @param matriz - matriz guardada em memória
     * @param size - dimensões da matriz
     * @return vector de inteiros com todos os valores da matriz
     */
    public static int[] matrizParaVetor(int matriz[][], int size) {
        int[] vetor = new int[size * size];
        int cont = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                vetor[cont] = matriz[i][j];
                cont = cont + 1;
            }
        }
        return vetor;
    }

    /**
     * Passa os valores por ordem para uma nova matriz e mostra a ao utilizador
     *
     * @param vetor - vetor com todos os valores da matriz ordenados
     * @param size - dimensões da matriz
     * @param matriz - matriz alterada pelos filtros
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @return matriz com o filtro já aplicado
     * @throws IOException
     */
    public static int[][] vetorParaMatriz(int[] vetor, int size, int[][] matriz, String nomeFich, String nome) throws IOException {
        int[][] matrizTransformada = new int[size][size];
        int contador = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrizTransformada[i][j] = vetor[contador];
                contador++;
            }
        }
        return matrizTransformada;
    }
    
    public static int [] pedirMatrizKernel(int [] vetorKernel) {
        int k = -1;
        System.out.format("%s%n","Qual a matriz Kernel a aplicar?");
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                System.out.format("%s%d%s%d%s%n","Valor da posição [",i,",",j,"]");
                String resposta = scan.nextLine();
                while (!resposta.matches("[-]?[0-9]+")) {
                    System.out.format("%s%n","Por favor insira um valor inteiro");
                    resposta = scan.nextLine();
                }
                k++;
                vetorKernel [k] = Integer.parseInt(resposta);
            }
        }
        return vetorKernel;
    }
    
    public static int filtroConvolucao(int [] vetorValores, int [] vetorKernel) {
        int valorConvolucao = 0;
        for (int i = 0; i < vetorValores.length; i++) {
            valorConvolucao = valorConvolucao + vetorValores[i]*vetorKernel[i];
        }
        if (valorConvolucao > 255) {
            valorConvolucao = 255;
        }
        if (valorConvolucao < 0) {
            valorConvolucao = 0;
        }
        return valorConvolucao;
    }

    /**
     * Mostra ao utilizador a matriz em questão
     *
     * @param matriz - matriz que vai ser mostrada ao utilizador
     */
    public static void listarMatriz(int[][] matriz) {
        System.out.println("Listagem da matriz");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.format("%3s%s", matriz[i][j], " ");
            }
            System.out.println();
        }
    }

    /**
     * Guarda num novo ficheiro a matriz alterada pelos filtros utilizados pelo
     * utilizador Dá ao ficheiro criado o nome do ficheiro original + o ultimo
     * filtro utilizado
     *
     * @param matriz - matriz em questão
     * @param size - dimensões da matriz
     * @param nomeFich - nome do ficheiro em questão
     * @param nome - nome da matriz (primeiro parametro da primeira linha do
     * ficheiro)
     * @throws IOException
     */
    public static void guardarNovoFicheiro(int[][] matriz, int size, String nomeFich, String nome) throws IOException {
        Formatter out = new Formatter(new File(nomeFich + ".txt"));
        out.format("%s", nome + "  " + size);
        out.format("%s%n%n", "");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                out.format("%s", matriz[i][j]);
                if (j != size - 1) {
                    out.format("%s", ",");
                }
            }
            if (i != size - 1) {
                out.format("%n%s", "");
            }
        }
        out.close();
    }
}
