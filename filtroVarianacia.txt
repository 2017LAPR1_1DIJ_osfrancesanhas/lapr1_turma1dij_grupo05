 public static int filtroVariancia(int vetorValores[]){
        int media=filtroMedia(vetorValores);
        int soma=0;
        for(int i=0;i<vetorValores.length;i++){
            soma=soma+ (int)(Math.pow(vetorValores[i]-media, 2));
        }
        return soma/9;
    }